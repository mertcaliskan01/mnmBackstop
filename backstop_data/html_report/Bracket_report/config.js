report({
  "testSuite": "BackstopJS",
  "tests": [
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Bracket_Divisions_Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_1Bracket_Divisions_Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Bracket_Divisions_Page_0_document_0_Desktop.png",
        "label": "1.Bracket Divisions Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Bracket/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00",
          "analysisTime": 404
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Bracket_Divisions_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_1Bracket_Divisions_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Bracket_Divisions_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "label": "1.Bracket Divisions Page CLICK ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Bracket/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00",
          "analysisTime": 393
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Bracket_Divisions_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_1Bracket_Divisions_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Bracket_Divisions_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "label": "1.Bracket Divisions Page HOVER ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Bracket/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00",
          "analysisTime": 399
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Bracket_Divisions_Page_HOVER_NewDivision_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_1Bracket_Divisions_Page_HOVER_NewDivision_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Bracket_Divisions_Page_HOVER_NewDivision_0_document_0_Desktop.png",
        "label": "1.Bracket Divisions Page HOVER NewDivision",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Bracket/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00",
          "analysisTime": 401
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Bracket_Divisions_ADD_Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_2Bracket_Divisions_ADD_Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Bracket_Divisions_ADD_Page_0_document_0_Desktop.png",
        "label": "2.Bracket Divisions ADD Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00",
          "analysisTime": 433
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Bracket_Divisions_ADD_Page_CLICK_Division_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_2Bracket_Divisions_ADD_Page_CLICK_Division_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Bracket_Divisions_ADD_Page_CLICK_Division_0_document_0_Desktop.png",
        "label": "2.Bracket Divisions ADD Page CLICK Division",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.03",
          "analysisTime": 669
        },
        "diffImage": "../AllTest/TestDesktop/20190531-133903/failed_diff_Test_2Bracket_Divisions_ADD_Page_CLICK_Division_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Bracket_Divisions_ADD_Page_CLICK_BracketType_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_2Bracket_Divisions_ADD_Page_CLICK_BracketType_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Bracket_Divisions_ADD_Page_CLICK_BracketType_0_document_0_Desktop.png",
        "label": "2.Bracket Divisions ADD Page CLICK BracketType",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00",
          "analysisTime": 423
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Bracket_ADD_Page_CLICK_InfoBoxesANDToggle_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_2Bracket_ADD_Page_CLICK_InfoBoxesANDToggle_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Bracket_ADD_Page_CLICK_InfoBoxesANDToggle_0_document_0_Desktop.png",
        "label": "2.Bracket ADD Page CLICK InfoBoxesANDToggle",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00",
          "analysisTime": 433
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Bracket_ADD_Page_HOVER_SaveBtn_0_rowmnm-buttons_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_2Bracket_ADD_Page_HOVER_SaveBtn_0_rowmnm-buttons_0_Desktop.png",
        "selector": ".row.mnm-buttons",
        "fileName": "Test_2Bracket_ADD_Page_HOVER_SaveBtn_0_rowmnm-buttons_0_Desktop.png",
        "label": "2.Bracket ADD Page HOVER SaveBtn",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Bracket_ADD_Page_HOVER_BackBtn_0_rowmnm-buttons_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_2Bracket_ADD_Page_HOVER_BackBtn_0_rowmnm-buttons_0_Desktop.png",
        "selector": ".row.mnm-buttons",
        "fileName": "Test_2Bracket_ADD_Page_HOVER_BackBtn_0_rowmnm-buttons_0_Desktop.png",
        "label": "2.Bracket ADD Page HOVER BackBtn",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Bracket_Divisions_ADD_Page_Empty_Error_Messages_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_2Bracket_Divisions_ADD_Page_Empty_Error_Messages_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Bracket_Divisions_ADD_Page_Empty_Error_Messages_0_document_0_Desktop.png",
        "label": "2.Bracket Divisions ADD Page Empty Error Messages",
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00",
          "analysisTime": 434
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_3Bracket_Fixture_Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_3Bracket_Fixture_Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_3Bracket_Fixture_Page_0_document_0_Desktop.png",
        "label": "3.Bracket Fixture Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketFixture/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00",
          "analysisTime": 415
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_3Bracket_Fixture_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_3Bracket_Fixture_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_3Bracket_Fixture_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "label": "3.Bracket Fixture Page CLICK ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketFixture/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00",
          "analysisTime": 383
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_3Bracket_Fixture_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_3Bracket_Fixture_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_3Bracket_Fixture_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "label": "3.Bracket Fixture Page HOVER ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketFixture/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00",
          "analysisTime": 414
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_3Bracket_Fixture_Page_HOVER_NewFixture_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_3Bracket_Fixture_Page_HOVER_NewFixture_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_3Bracket_Fixture_Page_HOVER_NewFixture_0_document_0_Desktop.png",
        "label": "3.Bracket Fixture Page HOVER NewFixture",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketFixture/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00",
          "analysisTime": 429
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Bracket_Fixture_ADD_Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_4Bracket_Fixture_ADD_Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Bracket_Fixture_ADD_Page_0_document_0_Desktop.png",
        "label": "4.Bracket Fixture ADD Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketFixtureAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.07",
          "analysisTime": 601
        },
        "diffImage": "../AllTest/TestDesktop/20190531-133903/failed_diff_Test_4Bracket_Fixture_ADD_Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Bracket_Fixture_ADD_Page_CLICK_EventBracket_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_4Bracket_Fixture_ADD_Page_CLICK_EventBracket_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Bracket_Fixture_ADD_Page_CLICK_EventBracket_0_document_0_Desktop.png",
        "label": "4.Bracket Fixture ADD Page CLICK EventBracket",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketFixtureAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "1.20",
          "analysisTime": 821
        },
        "diffImage": "../AllTest/TestDesktop/20190531-133903/failed_diff_Test_4Bracket_Fixture_ADD_Page_CLICK_EventBracket_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Bracket_Fixture_ADD_Page_CLICK_Round_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_4Bracket_Fixture_ADD_Page_CLICK_Round_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Bracket_Fixture_ADD_Page_CLICK_Round_0_document_0_Desktop.png",
        "label": "4.Bracket Fixture ADD Page CLICK Round",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketFixtureAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00",
          "analysisTime": 595
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Bracket_Fixture_ADD_Page_CLICK_Match_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_4Bracket_Fixture_ADD_Page_CLICK_Match_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Bracket_Fixture_ADD_Page_CLICK_Match_0_document_0_Desktop.png",
        "label": "4.Bracket Fixture ADD Page CLICK Match",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketFixtureAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00",
          "analysisTime": 379
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Bracket_Fixture_ADD_Page_CLICK_Schedule_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_4Bracket_Fixture_ADD_Page_CLICK_Schedule_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Bracket_Fixture_ADD_Page_CLICK_Schedule_0_document_0_Desktop.png",
        "label": "4.Bracket Fixture ADD Page CLICK Schedule",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketFixtureAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00",
          "analysisTime": 401
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Bracket_Fixture_ADD_Page_CLICK_Area_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_4Bracket_Fixture_ADD_Page_CLICK_Area_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Bracket_Fixture_ADD_Page_CLICK_Area_0_document_0_Desktop.png",
        "label": "4.Bracket Fixture ADD Page CLICK Area",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketFixtureAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.20",
          "analysisTime": 531
        },
        "diffImage": "../AllTest/TestDesktop/20190531-133903/failed_diff_Test_4Bracket_Fixture_ADD_Page_CLICK_Area_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Bracket_Fixture_ADD_Page_CLICK_StartDateTime_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_4Bracket_Fixture_ADD_Page_CLICK_StartDateTime_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Bracket_Fixture_ADD_Page_CLICK_StartDateTime_0_document_0_Desktop.png",
        "label": "4.Bracket Fixture ADD Page CLICK StartDateTime",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.7,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketFixtureAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.11",
          "analysisTime": 482
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Bracket_Fixture_ADD_Page_CLICK_InfoBoxes_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_4Bracket_Fixture_ADD_Page_CLICK_InfoBoxes_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Bracket_Fixture_ADD_Page_CLICK_InfoBoxes_0_document_0_Desktop.png",
        "label": "4.Bracket Fixture ADD Page CLICK InfoBoxes",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketFixtureAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00",
          "analysisTime": 304
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Bracket_Fixture_ADD_Page_HOVER_SaveBtn_0_rowmnm-buttons_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_4Bracket_Fixture_ADD_Page_HOVER_SaveBtn_0_rowmnm-buttons_0_Desktop.png",
        "selector": ".row.mnm-buttons",
        "fileName": "Test_4Bracket_Fixture_ADD_Page_HOVER_SaveBtn_0_rowmnm-buttons_0_Desktop.png",
        "label": "4.Bracket Fixture ADD Page HOVER SaveBtn",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketFixtureAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Bracket_Fixture_ADD_Page_HOVER_BackBtn_0_rowmnm-buttons_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_4Bracket_Fixture_ADD_Page_HOVER_BackBtn_0_rowmnm-buttons_0_Desktop.png",
        "selector": ".row.mnm-buttons",
        "fileName": "Test_4Bracket_Fixture_ADD_Page_HOVER_BackBtn_0_rowmnm-buttons_0_Desktop.png",
        "label": "4.Bracket Fixture ADD Page HOVER BackBtn",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketFixtureAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Bracket_Fixture_ADD_Page_Empty_Error_Messages_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-133903/Test_4Bracket_Fixture_ADD_Page_Empty_Error_Messages_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Bracket_Fixture_ADD_Page_Empty_Error_Messages_0_document_0_Desktop.png",
        "label": "4.Bracket Fixture ADD Page Empty Error Messages",
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/BracketFixtureAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00",
          "analysisTime": 143
        }
      },
      "status": "pass"
    }
  ],
  "id": "Test"
});