report({
  "testSuite": "BackstopJS",
  "tests": [
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Participants__Total__Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_1Participants__Total__Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Participants__Total__Page_0_document_0_Desktop.png",
        "label": "1.Participants  Total  Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "15.63",
          "analysisTime": 792
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_1Participants__Total__Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Participants__Total__Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_1Participants__Total__Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Participants__Total__Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png",
        "label": "1.Participants  Total  Page CLICK Indicatör NameAreas",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.44",
          "analysisTime": 602
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_1Participants__Total__Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Participants__Total__Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_1Participants__Total__Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Participants__Total__Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "label": "1.Participants  Total  Page CLICK ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.33",
          "analysisTime": 552
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_1Participants__Total__Page_CLICK_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Participants__Total__Page_Hover_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_1Participants__Total__Page_Hover_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Participants__Total__Page_Hover_ColumnChooser_0_document_0_Desktop.png",
        "label": "1.Participants  Total  Page Hover ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.15",
          "analysisTime": 581
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_1Participants__Total__Page_Hover_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Participants__Total__Page_Hover_AddParticipant_Btn_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_1Participants__Total__Page_Hover_AddParticipant_Btn_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Participants__Total__Page_Hover_AddParticipant_Btn_0_document_0_Desktop.png",
        "label": "1.Participants  Total  Page Hover AddParticipant Btn",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.08",
          "analysisTime": 663
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_1Participants__Total__Page_Hover_AddParticipant_Btn_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Participants__Approved__Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_2Participants__Approved__Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Participants__Approved__Page_0_document_0_Desktop.png",
        "label": "2.Participants  Approved  Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?id=29&status=1",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.10",
          "analysisTime": 686
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_2Participants__Approved__Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Participants__Approved__Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_2Participants__Approved__Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Participants__Approved__Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png",
        "label": "2.Participants  Approved  Page CLICK Indicatör NameAreas",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?id=29&status=1",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.42",
          "analysisTime": 613
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_2Participants__Approved__Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Participants__Approved__Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_2Participants__Approved__Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Participants__Approved__Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "label": "2.Participants  Approved  Page CLICK ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?id=29&status=1",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.74",
          "analysisTime": 620
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_2Participants__Approved__Page_CLICK_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Participants__Approved__Page_Hover_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_2Participants__Approved__Page_Hover_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Participants__Approved__Page_Hover_ColumnChooser_0_document_0_Desktop.png",
        "label": "2.Participants  Approved  Page Hover ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?id=29&status=1",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.18",
          "analysisTime": 595
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_2Participants__Approved__Page_Hover_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Participants__Approved__Page_Hover_AddParticipant_Btn_0__0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_2Participants__Approved__Page_Hover_AddParticipant_Btn_0__0_Desktop.png",
        "selector": "",
        "fileName": "Test_2Participants__Approved__Page_Hover_AddParticipant_Btn_0__0_Desktop.png",
        "label": "2.Participants  Approved  Page Hover AddParticipant Btn",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?id=29&status=1",
        "expect": 0,
        "viewportLabel": "Desktop",
        "engineErrorMsg": "waiting for selector \".btn.mnm-green\" failed: timeout 30000ms exceeded",
        "error": "Reference file not found /home/mert/Desktop/ajax/backstop_data/html_report/AllTest/ReferenceDesktop/Test_2Participants__Approved__Page_Hover_AddParticipant_Btn_0__0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_3Participants__Pending__Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_3Participants__Pending__Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_3Participants__Pending__Page_0_document_0_Desktop.png",
        "label": "3.Participants  Pending  Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?id=29&status=0",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.09",
          "analysisTime": 758
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_3Participants__Pending__Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_3Participants__Pending__Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_3Participants__Pending__Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_3Participants__Pending__Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png",
        "label": "3.Participants  Pending  Page CLICK Indicatör NameAreas",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?id=29&status=0",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.41",
          "analysisTime": 573
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_3Participants__Pending__Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_3Participants__Pending__Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_3Participants__Pending__Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_3Participants__Pending__Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "label": "3.Participants  Pending  Page CLICK ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?id=29&status=0",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.12",
          "analysisTime": 628
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_3Participants__Pending__Page_CLICK_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_3Participants__Pending__Page_Hover_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_3Participants__Pending__Page_Hover_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_3Participants__Pending__Page_Hover_ColumnChooser_0_document_0_Desktop.png",
        "label": "3.Participants  Pending  Page Hover ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?id=29&status=0",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.17",
          "analysisTime": 683
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_3Participants__Pending__Page_Hover_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_3Participants__Pending__Page_Hover_AddParticipant_Btn_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_3Participants__Pending__Page_Hover_AddParticipant_Btn_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_3Participants__Pending__Page_Hover_AddParticipant_Btn_0_document_0_Desktop.png",
        "label": "3.Participants  Pending  Page Hover AddParticipant Btn",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?id=29&status=0",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.10",
          "analysisTime": 661
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_3Participants__Pending__Page_Hover_AddParticipant_Btn_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Participants__Rejected__Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_4Participants__Rejected__Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Participants__Rejected__Page_0_document_0_Desktop.png",
        "label": "4.Participants  Rejected  Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?id=29&status=2",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.76",
          "analysisTime": 605
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_4Participants__Rejected__Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Participants__Rejected__Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_4Participants__Rejected__Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Participants__Rejected__Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "label": "4.Participants  Rejected  Page CLICK ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?id=29&status=2",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.24",
          "analysisTime": 621
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_4Participants__Rejected__Page_CLICK_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Participants__Rejected__Page_Hover_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_4Participants__Rejected__Page_Hover_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Participants__Rejected__Page_Hover_ColumnChooser_0_document_0_Desktop.png",
        "label": "4.Participants  Rejected  Page Hover ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?id=29&status=2",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.84",
          "analysisTime": 744
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_4Participants__Rejected__Page_Hover_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Participants__Rejected__Page_Hover_AddParticipant_Btn_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_4Participants__Rejected__Page_Hover_AddParticipant_Btn_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Participants__Rejected__Page_Hover_AddParticipant_Btn_0_document_0_Desktop.png",
        "label": "4.Participants  Rejected  Page Hover AddParticipant Btn",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?id=29&status=2",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.77",
          "analysisTime": 627
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_4Participants__Rejected__Page_Hover_AddParticipant_Btn_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_5Participants__CheckedIn__Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_5Participants__CheckedIn__Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_5Participants__CheckedIn__Page_0_document_0_Desktop.png",
        "label": "5.Participants  CheckedIn  Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?checked-in=True",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.93",
          "analysisTime": 618
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_5Participants__CheckedIn__Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_5Participants__CheckedIn__Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_5Participants__CheckedIn__Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_5Participants__CheckedIn__Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "label": "5.Participants  CheckedIn  Page CLICK ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?checked-in=True",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.40",
          "analysisTime": 646
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_5Participants__CheckedIn__Page_CLICK_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_5Participants__CheckedIn__Page_Hover_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_5Participants__CheckedIn__Page_Hover_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_5Participants__CheckedIn__Page_Hover_ColumnChooser_0_document_0_Desktop.png",
        "label": "5.Participants  CheckedIn  Page Hover ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?checked-in=True",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.00",
          "analysisTime": 641
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_5Participants__CheckedIn__Page_Hover_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_5Participants__CheckedIn__Page_Hover_AddParticipant_Btn_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_5Participants__CheckedIn__Page_Hover_AddParticipant_Btn_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_5Participants__CheckedIn__Page_Hover_AddParticipant_Btn_0_document_0_Desktop.png",
        "label": "5.Participants  CheckedIn  Page Hover AddParticipant Btn",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?checked-in=True",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.94",
          "analysisTime": 696
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_5Participants__CheckedIn__Page_Hover_AddParticipant_Btn_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_6Participants__WeighedIn__Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_6Participants__WeighedIn__Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_6Participants__WeighedIn__Page_0_document_0_Desktop.png",
        "label": "6.Participants  WeighedIn  Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?weighed-in=True",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.92",
          "analysisTime": 644
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_6Participants__WeighedIn__Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_6Participants__WeighedIn__Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_6Participants__WeighedIn__Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_6Participants__WeighedIn__Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "label": "6.Participants  WeighedIn  Page CLICK ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?weighed-in=True",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.40",
          "analysisTime": 622
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_6Participants__WeighedIn__Page_CLICK_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_6Participants__WeighedIn__Page_Hover_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_6Participants__WeighedIn__Page_Hover_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_6Participants__WeighedIn__Page_Hover_ColumnChooser_0_document_0_Desktop.png",
        "label": "6.Participants  WeighedIn  Page Hover ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?weighed-in=True",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.00",
          "analysisTime": 658
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_6Participants__WeighedIn__Page_Hover_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_6Participants__WeighedIn__Page_Hover_AddParticipant_Btn_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_6Participants__WeighedIn__Page_Hover_AddParticipant_Btn_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_6Participants__WeighedIn__Page_Hover_AddParticipant_Btn_0_document_0_Desktop.png",
        "label": "6.Participants  WeighedIn  Page Hover AddParticipant Btn",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Participant/29?weighed-in=True",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.94",
          "analysisTime": 622
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_6Participants__WeighedIn__Page_Hover_AddParticipant_Btn_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_7Participants__Add__Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_7Participants__Add__Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_7Participants__Add__Page_0_document_0_Desktop.png",
        "label": "7.Participants  Add  Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.03,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -26
          },
          "misMatchPercentage": "8.60",
          "analysisTime": 675
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_7Participants__Add__Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_7Participants__Add__Page_CLICK_User_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_7Participants__Add__Page_CLICK_User_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_7Participants__Add__Page_CLICK_User_0_document_0_Desktop.png",
        "label": "7.Participants  Add  Page CLICK User",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -26
          },
          "misMatchPercentage": "6.67",
          "analysisTime": 707
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_7Participants__Add__Page_CLICK_User_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_7Participants__Add__Page_CLICK_Organisation_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_7Participants__Add__Page_CLICK_Organisation_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_7Participants__Add__Page_CLICK_Organisation_0_document_0_Desktop.png",
        "label": "7.Participants  Add  Page CLICK Organisation",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.01,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -26
          },
          "misMatchPercentage": "9.00",
          "analysisTime": 554
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_7Participants__Add__Page_CLICK_Organisation_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_7Participants__Add__Page_CLICK_UserRole_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_7Participants__Add__Page_CLICK_UserRole_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_7Participants__Add__Page_CLICK_UserRole_0_document_0_Desktop.png",
        "label": "7.Participants  Add  Page CLICK UserRole",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.01,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -26
          },
          "misMatchPercentage": "8.47",
          "analysisTime": 637
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_7Participants__Add__Page_CLICK_UserRole_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_7Participants__Add__Page_CLICK_Division_0__0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_7Participants__Add__Page_CLICK_Division_0__0_Desktop.png",
        "selector": "",
        "fileName": "Test_7Participants__Add__Page_CLICK_Division_0__0_Desktop.png",
        "label": "7.Participants  Add  Page CLICK Division",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.03,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "engineErrorMsg": "waiting for selector \"#EventParticipant_SportDivisionId\" failed: timeout 30000ms exceeded",
        "error": "Reference file not found /home/mert/Desktop/ajax/backstop_data/html_report/AllTest/ReferenceDesktop/Test_7Participants__Add__Page_CLICK_Division_0__0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_7Participants__Add__Page_CLICK_EventBracket_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_7Participants__Add__Page_CLICK_EventBracket_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_7Participants__Add__Page_CLICK_EventBracket_0_document_0_Desktop.png",
        "label": "7.Participants  Add  Page CLICK EventBracket",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.03,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -26
          },
          "misMatchPercentage": "9.25",
          "analysisTime": 204
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_7Participants__Add__Page_CLICK_EventBracket_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_7Participants__Add__Page_CLICK_EventTicket_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_7Participants__Add__Page_CLICK_EventTicket_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_7Participants__Add__Page_CLICK_EventTicket_0_document_0_Desktop.png",
        "label": "7.Participants  Add  Page CLICK EventTicket",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -26
          },
          "misMatchPercentage": "8.87",
          "analysisTime": 209
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_7Participants__Add__Page_CLICK_EventTicket_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_7Participants__Add__Page_CLICK_ParticipantStatus_0_viewport_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_7Participants__Add__Page_CLICK_ParticipantStatus_0_viewport_0_Desktop.png",
        "selector": "viewport",
        "fileName": "Test_7Participants__Add__Page_CLICK_ParticipantStatus_0_viewport_0_Desktop.png",
        "label": "7.Participants  Add  Page CLICK ParticipantStatus",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.03,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.54",
          "analysisTime": 148
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_7Participants__Add__Page_CLICK_ParticipantStatus_0_viewport_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_7Participants__Add__Page_CLICK_AppliedDate_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_7Participants__Add__Page_CLICK_AppliedDate_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_7Participants__Add__Page_CLICK_AppliedDate_0_document_0_Desktop.png",
        "label": "7.Participants  Add  Page CLICK AppliedDate",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.2,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -26
          },
          "misMatchPercentage": "9.28",
          "analysisTime": 221
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_7Participants__Add__Page_CLICK_AppliedDate_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_7Participants__Add__Page_HOVER_Back_0_rowmnm-buttons_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_7Participants__Add__Page_HOVER_Back_0_rowmnm-buttons_0_Desktop.png",
        "selector": ".row.mnm-buttons",
        "fileName": "Test_7Participants__Add__Page_HOVER_Back_0_rowmnm-buttons_0_Desktop.png",
        "label": "7.Participants  Add  Page HOVER Back",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.01,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_7Participants__Add__Page_HOVER_Save_0_rowmnm-buttons_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_7Participants__Add__Page_HOVER_Save_0_rowmnm-buttons_0_Desktop.png",
        "selector": ".row.mnm-buttons",
        "fileName": "Test_7Participants__Add__Page_HOVER_Save_0_rowmnm-buttons_0_Desktop.png",
        "label": "7.Participants  Add  Page HOVER Save",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.01,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_7Participants__Add__Page_CLICK_Info_Boxes_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_7Participants__Add__Page_CLICK_Info_Boxes_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_7Participants__Add__Page_CLICK_Info_Boxes_0_document_0_Desktop.png",
        "label": "7.Participants  Add  Page CLICK [Info Boxes]",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.03,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -26
          },
          "misMatchPercentage": "10.14",
          "analysisTime": 181
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_7Participants__Add__Page_CLICK_Info_Boxes_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_7Participants_Page_NewVenue_Empty_Error_Messages_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233432/Test_7Participants_Page_NewVenue_Empty_Error_Messages_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_7Participants_Page_NewVenue_Empty_Error_Messages_0_document_0_Desktop.png",
        "label": "7.Participants Page NewVenue Empty Error Messages",
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -26
          },
          "misMatchPercentage": "8.76",
          "analysisTime": 230
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233432/failed_diff_Test_7Participants_Page_NewVenue_Empty_Error_Messages_0_document_0_Desktop.png"
      },
      "status": "fail"
    }
  ],
  "id": "Test"
});