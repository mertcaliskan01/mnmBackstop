report({
  "testSuite": "BackstopJS",
  "tests": [
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Award_Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_1Award_Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Award_Page_0_document_0_Desktop.png",
        "label": "1.Award Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Award/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "6.47",
          "analysisTime": 634
        },
        "diffImage": "../AllTest/TestDesktop/20190620-070119/failed_diff_Test_1Award_Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Award_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_1Award_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Award_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "label": "1.Award Page CLICK ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Award/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "6.19",
          "analysisTime": 647
        },
        "diffImage": "../AllTest/TestDesktop/20190620-070119/failed_diff_Test_1Award_Page_CLICK_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Award_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_1Award_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Award_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "label": "1.Award Page HOVER ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Award/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "6.52",
          "analysisTime": 626
        },
        "diffImage": "../AllTest/TestDesktop/20190620-070119/failed_diff_Test_1Award_Page_HOVER_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Award_Page_HOVER_AddAward_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_1Award_Page_HOVER_AddAward_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Award_Page_HOVER_AddAward_0_document_0_Desktop.png",
        "label": "1.Award Page HOVER AddAward",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Award/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "6.50",
          "analysisTime": 643
        },
        "diffImage": "../AllTest/TestDesktop/20190620-070119/failed_diff_Test_1Award_Page_HOVER_AddAward_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Award_ADD_Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_2Award_ADD_Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Award_ADD_Page_0_document_0_Desktop.png",
        "label": "2.Award ADD Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/AwardAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.26",
          "analysisTime": 639
        },
        "diffImage": "../AllTest/TestDesktop/20190620-070119/failed_diff_Test_2Award_ADD_Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Award_ADD_Page_CLICK_Icon_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_2Award_ADD_Page_CLICK_Icon_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Award_ADD_Page_CLICK_Icon_0_document_0_Desktop.png",
        "label": "2.Award ADD Page CLICK Icon",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/AwardAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.92",
          "analysisTime": 604
        },
        "diffImage": "../AllTest/TestDesktop/20190620-070119/failed_diff_Test_2Award_ADD_Page_CLICK_Icon_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Award_Page_HOVER_SaveBtn_0_rowmnm-buttons_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_2Award_Page_HOVER_SaveBtn_0_rowmnm-buttons_0_Desktop.png",
        "selector": ".row.mnm-buttons",
        "fileName": "Test_2Award_Page_HOVER_SaveBtn_0_rowmnm-buttons_0_Desktop.png",
        "label": "2.Award Page HOVER SaveBtn",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/AwardAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Award_Page_HOVER_BackBtn_0_rowmnm-buttons_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_2Award_Page_HOVER_BackBtn_0_rowmnm-buttons_0_Desktop.png",
        "selector": ".row.mnm-buttons",
        "fileName": "Test_2Award_Page_HOVER_BackBtn_0_rowmnm-buttons_0_Desktop.png",
        "label": "2.Award Page HOVER BackBtn",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/AwardAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Award_ADD_Page_CLICK_InfoBoxesANDToggle_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_2Award_ADD_Page_CLICK_InfoBoxesANDToggle_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Award_ADD_Page_CLICK_InfoBoxesANDToggle_0_document_0_Desktop.png",
        "label": "2.Award ADD Page CLICK InfoBoxesANDToggle",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/AwardAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.57",
          "analysisTime": 599
        },
        "diffImage": "../AllTest/TestDesktop/20190620-070119/failed_diff_Test_2Award_ADD_Page_CLICK_InfoBoxesANDToggle_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Award_ADD_Page_Empty_Error_Messages_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_2Award_ADD_Page_Empty_Error_Messages_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Award_ADD_Page_Empty_Error_Messages_0_document_0_Desktop.png",
        "label": "2.Award ADD Page Empty Error Messages",
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/AwardAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.41",
          "analysisTime": 604
        },
        "diffImage": "../AllTest/TestDesktop/20190620-070119/failed_diff_Test_2Award_ADD_Page_Empty_Error_Messages_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_3Participant_Award_Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_3Participant_Award_Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_3Participant_Award_Page_0_document_0_Desktop.png",
        "label": "3.Participant Award Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAward/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "6.69",
          "analysisTime": 616
        },
        "diffImage": "../AllTest/TestDesktop/20190620-070119/failed_diff_Test_3Participant_Award_Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_3Participant_Award_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_3Participant_Award_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_3Participant_Award_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "label": "3.Participant Award Page CLICK ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAward/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "6.29",
          "analysisTime": 622
        },
        "diffImage": "../AllTest/TestDesktop/20190620-070119/failed_diff_Test_3Participant_Award_Page_CLICK_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_3Participant_Award_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_3Participant_Award_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_3Participant_Award_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "label": "3.Participant Award Page HOVER ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAward/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "6.70",
          "analysisTime": 607
        },
        "diffImage": "../AllTest/TestDesktop/20190620-070119/failed_diff_Test_3Participant_Award_Page_HOVER_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_3Participant_Award_Page_HOVER_NewDivision_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_3Participant_Award_Page_HOVER_NewDivision_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_3Participant_Award_Page_HOVER_NewDivision_0_document_0_Desktop.png",
        "label": "3.Participant Award Page HOVER NewDivision",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAward/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "6.68",
          "analysisTime": 620
        },
        "diffImage": "../AllTest/TestDesktop/20190620-070119/failed_diff_Test_3Participant_Award_Page_HOVER_NewDivision_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Participant_Award_Add_Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_4Participant_Award_Add_Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Participant_Award_Add_Page_0_document_0_Desktop.png",
        "label": "4.Participant Award Add Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAwardAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.21",
          "analysisTime": 603
        },
        "diffImage": "../AllTest/TestDesktop/20190620-070119/failed_diff_Test_4Participant_Award_Add_Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Participant_Award_Add_Page_CLICK_Award_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_4Participant_Award_Add_Page_CLICK_Award_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Participant_Award_Add_Page_CLICK_Award_0_document_0_Desktop.png",
        "label": "4.Participant Award Add Page CLICK Award",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAwardAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.95",
          "analysisTime": 583
        },
        "diffImage": "../AllTest/TestDesktop/20190620-070119/failed_diff_Test_4Participant_Award_Add_Page_CLICK_Award_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Participant_Award_Add_Page_CLICK_Division_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_4Participant_Award_Add_Page_CLICK_Division_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Participant_Award_Add_Page_CLICK_Division_0_document_0_Desktop.png",
        "label": "4.Participant Award Add Page CLICK Division",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAwardAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.65",
          "analysisTime": 578
        },
        "diffImage": "../AllTest/TestDesktop/20190620-070119/failed_diff_Test_4Participant_Award_Add_Page_CLICK_Division_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Participant_Award_Add_Page_CLICK_Participant_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_4Participant_Award_Add_Page_CLICK_Participant_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Participant_Award_Add_Page_CLICK_Participant_0_document_0_Desktop.png",
        "label": "4.Participant Award Add Page CLICK Participant",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAwardAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.06",
          "analysisTime": 352
        },
        "diffImage": "../AllTest/TestDesktop/20190620-070119/failed_diff_Test_4Participant_Award_Add_Page_CLICK_Participant_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Participant_Award_Page_HOVER_SaveBtn_0_rowmnm-buttons_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_4Participant_Award_Page_HOVER_SaveBtn_0_rowmnm-buttons_0_Desktop.png",
        "selector": ".row.mnm-buttons",
        "fileName": "Test_4Participant_Award_Page_HOVER_SaveBtn_0_rowmnm-buttons_0_Desktop.png",
        "label": "4.Participant Award Page HOVER SaveBtn",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAwardAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Participant_Award_Page_HOVER_BackBtn_0_rowmnm-buttons_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_4Participant_Award_Page_HOVER_BackBtn_0_rowmnm-buttons_0_Desktop.png",
        "selector": ".row.mnm-buttons",
        "fileName": "Test_4Participant_Award_Page_HOVER_BackBtn_0_rowmnm-buttons_0_Desktop.png",
        "label": "4.Participant Award Page HOVER BackBtn",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAwardAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Participant_Award_Add_Page_CLICK_InfoBoxes_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_4Participant_Award_Add_Page_CLICK_InfoBoxes_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Participant_Award_Add_Page_CLICK_InfoBoxes_0_document_0_Desktop.png",
        "label": "4.Participant Award Add Page CLICK InfoBoxes",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAwardAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.80",
          "analysisTime": 368
        },
        "diffImage": "../AllTest/TestDesktop/20190620-070119/failed_diff_Test_4Participant_Award_Add_Page_CLICK_InfoBoxes_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Participant_Award_Add_Page_Empty_Error_Messages_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190620-070119/Test_4Participant_Award_Add_Page_Empty_Error_Messages_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Participant_Award_Add_Page_Empty_Error_Messages_0_document_0_Desktop.png",
        "label": "4.Participant Award Add Page Empty Error Messages",
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ParticipantAwardAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.36",
          "analysisTime": 362
        },
        "diffImage": "../AllTest/TestDesktop/20190620-070119/failed_diff_Test_4Participant_Award_Add_Page_Empty_Error_Messages_0_document_0_Desktop.png"
      },
      "status": "fail"
    }
  ],
  "id": "Test"
});