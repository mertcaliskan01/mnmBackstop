report({
  "testSuite": "BackstopJS",
  "tests": [
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_Event_Profile__Event_Info_Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_Event_Profile__Event_Info_Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_Event_Profile__Event_Info_Page_0_document_0_Desktop.png",
        "label": "Event Profile  Event Info Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/Info/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -127
          },
          "misMatchPercentage": "25.82",
          "analysisTime": 1430
        },
        "diffImage": "../AllTest/TestDesktop/20190601-093322/failed_diff_Test_Event_Profile__Event_Info_Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Event_Profile__ScheduleVenue_Page_0__0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_2Event_Profile__ScheduleVenue_Page_0__0_Desktop.png",
        "selector": "",
        "fileName": "Test_2Event_Profile__ScheduleVenue_Page_0__0_Desktop.png",
        "label": "2.Event Profile  ScheduleVenue Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/ScheduleAndVenue/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "engineErrorMsg": "waiting for selector \"#headingOne_62\" failed: timeout 30000ms exceeded",
        "error": "Reference file not found /srv/www/mnm/backstop_data/html_report/AllTest/ReferenceDesktop/Test_2Event_Profile__ScheduleVenue_Page_0__0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Event_Profile__ScheduleVenue_Page_CLICK_SHOW_0__0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_2Event_Profile__ScheduleVenue_Page_CLICK_SHOW_0__0_Desktop.png",
        "selector": "",
        "fileName": "Test_2Event_Profile__ScheduleVenue_Page_CLICK_SHOW_0__0_Desktop.png",
        "label": "2.Event Profile  ScheduleVenue Page CLICK SHOW",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/ScheduleAndVenue/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "engineErrorMsg": "waiting for selector \"#headingOne_62\" failed: timeout 30000ms exceeded",
        "error": "Reference file not found /srv/www/mnm/backstop_data/html_report/AllTest/ReferenceDesktop/Test_2Event_Profile__ScheduleVenue_Page_CLICK_SHOW_0__0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Event_Profile__ScheduleVenue_Page_HOVER_SearchBtn_0_show_white_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_2Event_Profile__ScheduleVenue_Page_HOVER_SearchBtn_0_show_white_0_Desktop.png",
        "selector": ".show_white",
        "fileName": "Test_2Event_Profile__ScheduleVenue_Page_HOVER_SearchBtn_0_show_white_0_Desktop.png",
        "label": "2.Event Profile  ScheduleVenue Page HOVER SearchBtn",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/ScheduleAndVenue/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "5.06",
          "analysisTime": 214
        },
        "diffImage": "../AllTest/TestDesktop/20190601-093322/failed_diff_Test_2Event_Profile__ScheduleVenue_Page_HOVER_SearchBtn_0_show_white_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Event_Profile__ScheduleVenue_Page_SHOW_Day1_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_2Event_Profile__ScheduleVenue_Page_SHOW_Day1_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Event_Profile__ScheduleVenue_Page_SHOW_Day1_0_document_0_Desktop.png",
        "label": "2.Event Profile  ScheduleVenue Page SHOW Day1",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/ScheduleAndVenue/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "27.22",
          "analysisTime": 671
        },
        "diffImage": "../AllTest/TestDesktop/20190601-093322/failed_diff_Test_2Event_Profile__ScheduleVenue_Page_SHOW_Day1_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Event_Profile__ScheduleVenue_Page_SHOW_DetailDay1_0__0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_2Event_Profile__ScheduleVenue_Page_SHOW_DetailDay1_0__0_Desktop.png",
        "selector": "",
        "fileName": "Test_2Event_Profile__ScheduleVenue_Page_SHOW_DetailDay1_0__0_Desktop.png",
        "label": "2.Event Profile  ScheduleVenue Page SHOW DetailDay1",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/ScheduleAndVenue/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "engineErrorMsg": "waiting for selector \"#accordionExample2\" failed: timeout 30000ms exceeded",
        "error": "Reference file not found /srv/www/mnm/backstop_data/html_report/AllTest/ReferenceDesktop/Test_2Event_Profile__ScheduleVenue_Page_SHOW_DetailDay1_0__0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_3Event_Profile__Participants_Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_3Event_Profile__Participants_Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_3Event_Profile__Participants_Page_0_document_0_Desktop.png",
        "label": "3.Event Profile  Participants Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/Participants/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "26.67",
          "analysisTime": 688
        },
        "diffImage": "../AllTest/TestDesktop/20190601-093322/failed_diff_Test_3Event_Profile__Participants_Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Event_Profile__Brackets_Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_4Event_Profile__Brackets_Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Event_Profile__Brackets_Page_0_document_0_Desktop.png",
        "label": "4.Event Profile  Brackets Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/Brackets/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -36
          },
          "misMatchPercentage": "30.65",
          "analysisTime": 783
        },
        "diffImage": "../AllTest/TestDesktop/20190601-093322/failed_diff_Test_4Event_Profile__Brackets_Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Event_Profile__Brackets_Page_CLICK_Country_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_4Event_Profile__Brackets_Page_CLICK_Country_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Event_Profile__Brackets_Page_CLICK_Country_0_document_0_Desktop.png",
        "label": "4.Event Profile  Brackets Page CLICK Country",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/Brackets/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -36
          },
          "misMatchPercentage": "30.47",
          "analysisTime": 629
        },
        "diffImage": "../AllTest/TestDesktop/20190601-093322/failed_diff_Test_4Event_Profile__Brackets_Page_CLICK_Country_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Event_Profile__Brackets_Page_CLICK_Division_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_4Event_Profile__Brackets_Page_CLICK_Division_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Event_Profile__Brackets_Page_CLICK_Division_0_document_0_Desktop.png",
        "label": "4.Event Profile  Brackets Page CLICK Division",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/Brackets/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -36
          },
          "misMatchPercentage": "30.38",
          "analysisTime": 620
        },
        "diffImage": "../AllTest/TestDesktop/20190601-093322/failed_diff_Test_4Event_Profile__Brackets_Page_CLICK_Division_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Event_Profile__Brackets_Page_CLICK_Area_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_4Event_Profile__Brackets_Page_CLICK_Area_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_4Event_Profile__Brackets_Page_CLICK_Area_0_document_0_Desktop.png",
        "label": "4.Event Profile  Brackets Page CLICK Area",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/Brackets/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -36
          },
          "misMatchPercentage": "30.41",
          "analysisTime": 662
        },
        "diffImage": "../AllTest/TestDesktop/20190601-093322/failed_diff_Test_4Event_Profile__Brackets_Page_CLICK_Area_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Event_Profile__Brackets_Page_CLICK_Status_0_viewport_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_4Event_Profile__Brackets_Page_CLICK_Status_0_viewport_0_Desktop.png",
        "selector": "viewport",
        "fileName": "Test_4Event_Profile__Brackets_Page_CLICK_Status_0_viewport_0_Desktop.png",
        "label": "4.Event Profile  Brackets Page CLICK Status",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/Brackets/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "31.70",
          "analysisTime": 560
        },
        "diffImage": "../AllTest/TestDesktop/20190601-093322/failed_diff_Test_4Event_Profile__Brackets_Page_CLICK_Status_0_viewport_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_4Event_Profile__Brackets_Page_HOVER_Search_Box_0_match_white_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_4Event_Profile__Brackets_Page_HOVER_Search_Box_0_match_white_0_Desktop.png",
        "selector": ".match_white",
        "fileName": "Test_4Event_Profile__Brackets_Page_HOVER_Search_Box_0_match_white_0_Desktop.png",
        "label": "4.Event Profile  Brackets Page HOVER Search Box",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/Brackets/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.33",
          "analysisTime": 146
        },
        "diffImage": "../AllTest/TestDesktop/20190601-093322/failed_diff_Test_4Event_Profile__Brackets_Page_HOVER_Search_Box_0_match_white_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_5Event_Profile__Match_Results_Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_5Event_Profile__Match_Results_Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_5Event_Profile__Match_Results_Page_0_document_0_Desktop.png",
        "label": "5.Event Profile  Match Results Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/MatchResults/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -36
          },
          "misMatchPercentage": "30.65",
          "analysisTime": 751
        },
        "diffImage": "../AllTest/TestDesktop/20190601-093322/failed_diff_Test_5Event_Profile__Match_Results_Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_5Event_Profile__Match_Results_Page_CLICK_Country_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_5Event_Profile__Match_Results_Page_CLICK_Country_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_5Event_Profile__Match_Results_Page_CLICK_Country_0_document_0_Desktop.png",
        "label": "5.Event Profile  Match Results Page CLICK Country",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/MatchResults/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -36
          },
          "misMatchPercentage": "30.47",
          "analysisTime": 648
        },
        "diffImage": "../AllTest/TestDesktop/20190601-093322/failed_diff_Test_5Event_Profile__Match_Results_Page_CLICK_Country_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_5Event_Profile__Match_Results_Page_CLICK_Division_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_5Event_Profile__Match_Results_Page_CLICK_Division_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_5Event_Profile__Match_Results_Page_CLICK_Division_0_document_0_Desktop.png",
        "label": "5.Event Profile  Match Results Page CLICK Division",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/MatchResults/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -36
          },
          "misMatchPercentage": "30.38",
          "analysisTime": 667
        },
        "diffImage": "../AllTest/TestDesktop/20190601-093322/failed_diff_Test_5Event_Profile__Match_Results_Page_CLICK_Division_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_5Event_Profile__Match_Results_Page_CLICK_Area_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_5Event_Profile__Match_Results_Page_CLICK_Area_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_5Event_Profile__Match_Results_Page_CLICK_Area_0_document_0_Desktop.png",
        "label": "5.Event Profile  Match Results Page CLICK Area",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/MatchResults/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -36
          },
          "misMatchPercentage": "30.41",
          "analysisTime": 656
        },
        "diffImage": "../AllTest/TestDesktop/20190601-093322/failed_diff_Test_5Event_Profile__Match_Results_Page_CLICK_Area_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_5Event_Profile__Match_Results_Page_CLICK_Status_0_viewport_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_5Event_Profile__Match_Results_Page_CLICK_Status_0_viewport_0_Desktop.png",
        "selector": "viewport",
        "fileName": "Test_5Event_Profile__Match_Results_Page_CLICK_Status_0_viewport_0_Desktop.png",
        "label": "5.Event Profile  Match Results Page CLICK Status",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/MatchResults/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "31.70",
          "analysisTime": 592
        },
        "diffImage": "../AllTest/TestDesktop/20190601-093322/failed_diff_Test_5Event_Profile__Match_Results_Page_CLICK_Status_0_viewport_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_5Event_Profile__Match_Results_Page_HOVER_Search_Box_0_match_white_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_5Event_Profile__Match_Results_Page_HOVER_Search_Box_0_match_white_0_Desktop.png",
        "selector": ".match_white",
        "fileName": "Test_5Event_Profile__Match_Results_Page_HOVER_Search_Box_0_match_white_0_Desktop.png",
        "label": "5.Event Profile  Match Results Page HOVER Search Box",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/MatchResults/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.33",
          "analysisTime": 179
        },
        "diffImage": "../AllTest/TestDesktop/20190601-093322/failed_diff_Test_5Event_Profile__Match_Results_Page_HOVER_Search_Box_0_match_white_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_6Event_Profile__Awards_Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_6Event_Profile__Awards_Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_6Event_Profile__Awards_Page_0_document_0_Desktop.png",
        "label": "6.Event Profile  Awards Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/Awards/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -36
          },
          "misMatchPercentage": "30.62",
          "analysisTime": 515
        },
        "diffImage": "../AllTest/TestDesktop/20190601-093322/failed_diff_Test_6Event_Profile__Awards_Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_6Event_Profile__Awards_Page_CLICK_Country_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_6Event_Profile__Awards_Page_CLICK_Country_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_6Event_Profile__Awards_Page_CLICK_Country_0_document_0_Desktop.png",
        "label": "6.Event Profile  Awards Page CLICK Country",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/Awards/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -36
          },
          "misMatchPercentage": "30.08",
          "analysisTime": 524
        },
        "diffImage": "../AllTest/TestDesktop/20190601-093322/failed_diff_Test_6Event_Profile__Awards_Page_CLICK_Country_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_6Event_Profile__Awards_Page_CLICK_Division_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_6Event_Profile__Awards_Page_CLICK_Division_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_6Event_Profile__Awards_Page_CLICK_Division_0_document_0_Desktop.png",
        "label": "6.Event Profile  Awards Page CLICK Division",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/Awards/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -36
          },
          "misMatchPercentage": "30.33",
          "analysisTime": 567
        },
        "diffImage": "../AllTest/TestDesktop/20190601-093322/failed_diff_Test_6Event_Profile__Awards_Page_CLICK_Division_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_6Event_Profile__Awards_Page_CLICK_Organisation_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_6Event_Profile__Awards_Page_CLICK_Organisation_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_6Event_Profile__Awards_Page_CLICK_Organisation_0_document_0_Desktop.png",
        "label": "6.Event Profile  Awards Page CLICK Organisation",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/Awards/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -36
          },
          "misMatchPercentage": "30.97",
          "analysisTime": 573
        },
        "diffImage": "../AllTest/TestDesktop/20190601-093322/failed_diff_Test_6Event_Profile__Awards_Page_CLICK_Organisation_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_6Event_Profile__Awards_Page_HOVER_Search_Box_0_match_white_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_6Event_Profile__Awards_Page_HOVER_Search_Box_0_match_white_0_Desktop.png",
        "selector": ".match_white",
        "fileName": "Test_6Event_Profile__Awards_Page_HOVER_Search_Box_0_match_white_0_Desktop.png",
        "label": "6.Event Profile  Awards Page HOVER Search Box",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/Awards/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00",
          "analysisTime": 178
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_7OtherTicket_Individual_Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_7OtherTicket_Individual_Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_7OtherTicket_Individual_Page_0_document_0_Desktop.png",
        "label": "7.OtherTicket Individual Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/RegisterIndividual/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "error": "Reference file not found /srv/www/mnm/backstop_data/html_report/AllTest/ReferenceDesktop/Test_7OtherTicket_Individual_Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_7OtherTicket_Individual_Page_CLICK_NameAreaIndicatr_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_7OtherTicket_Individual_Page_CLICK_NameAreaIndicatr_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_7OtherTicket_Individual_Page_CLICK_NameAreaIndicatr_0_document_0_Desktop.png",
        "label": "7.OtherTicket Individual Page CLICK NameAreaIndicatör",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/RegisterIndividual/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "error": "Reference file not found /srv/www/mnm/backstop_data/html_report/AllTest/ReferenceDesktop/Test_7OtherTicket_Individual_Page_CLICK_NameAreaIndicatr_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_7OtherTicket_Individual_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_7OtherTicket_Individual_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_7OtherTicket_Individual_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "label": "7.OtherTicket Individual Page CLICK ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/RegisterIndividual/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "error": "Reference file not found /srv/www/mnm/backstop_data/html_report/AllTest/ReferenceDesktop/Test_7OtherTicket_Individual_Page_CLICK_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_7OtherTicket_Individual_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_7OtherTicket_Individual_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_7OtherTicket_Individual_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "label": "7.OtherTicket Individual Page HOVER ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/RegisterIndividual/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "error": "Reference file not found /srv/www/mnm/backstop_data/html_report/AllTest/ReferenceDesktop/Test_7OtherTicket_Individual_Page_HOVER_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_7OtherTicket_Organisation_Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_7OtherTicket_Organisation_Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_7OtherTicket_Organisation_Page_0_document_0_Desktop.png",
        "label": "7.OtherTicket Organisation Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/RegisterOrganisation/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "error": "Reference file not found /srv/www/mnm/backstop_data/html_report/AllTest/ReferenceDesktop/Test_7OtherTicket_Organisation_Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_7OtherTicket_Organisation_Page_CLICK_NameAreaIndicatr_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_7OtherTicket_Organisation_Page_CLICK_NameAreaIndicatr_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_7OtherTicket_Organisation_Page_CLICK_NameAreaIndicatr_0_document_0_Desktop.png",
        "label": "7.OtherTicket Organisation Page CLICK NameAreaIndicatör",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/RegisterOrganisation/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "error": "Reference file not found /srv/www/mnm/backstop_data/html_report/AllTest/ReferenceDesktop/Test_7OtherTicket_Organisation_Page_CLICK_NameAreaIndicatr_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_7OtherTicket_Organisation_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_7OtherTicket_Organisation_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_7OtherTicket_Organisation_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "label": "7.OtherTicket Organisation CLICK ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/RegisterOrganisation/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "error": "Reference file not found /srv/www/mnm/backstop_data/html_report/AllTest/ReferenceDesktop/Test_7OtherTicket_Organisation_CLICK_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_7OtherTicket_Organisation_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190601-093322/Test_7OtherTicket_Organisation_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_7OtherTicket_Organisation_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "label": "7.OtherTicket Organisation Page HOVER ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/Profile/RegisterOrganisation/44",
        "expect": 0,
        "viewportLabel": "Desktop",
        "error": "Reference file not found /srv/www/mnm/backstop_data/html_report/AllTest/ReferenceDesktop/Test_7OtherTicket_Organisation_Page_HOVER_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    }
  ],
  "id": "Test"
});