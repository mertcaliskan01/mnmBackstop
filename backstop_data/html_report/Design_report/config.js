report({
  "testSuite": "BackstopJS",
  "tests": [
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_Design_Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190621-193021/Test_Design_Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_Design_Page_0_document_0_Desktop.png",
        "label": "Design Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Design/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.06",
          "analysisTime": 178
        },
        "diffImage": "../AllTest/TestDesktop/20190621-193021/failed_diff_Test_Design_Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_Design_Page_Hover_ProfilPicture_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190621-193021/Test_Design_Page_Hover_ProfilPicture_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_Design_Page_Hover_ProfilPicture_0_document_0_Desktop.png",
        "label": "Design Page Hover ProfilPicture",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Design/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.06",
          "analysisTime": 154
        },
        "diffImage": "../AllTest/TestDesktop/20190621-193021/failed_diff_Test_Design_Page_Hover_ProfilPicture_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_Design_Page_Hover_InfoBox_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190621-193021/Test_Design_Page_Hover_InfoBox_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_Design_Page_Hover_InfoBox_0_document_0_Desktop.png",
        "label": "Design Page Hover InfoBox",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Design/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.24",
          "analysisTime": 146
        },
        "diffImage": "../AllTest/TestDesktop/20190621-193021/failed_diff_Test_Design_Page_Hover_InfoBox_0_document_0_Desktop.png"
      },
      "status": "fail"
    }
  ],
  "id": "Test"
});