report({
  "testSuite": "BackstopJS",
  "tests": [
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Venue_Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_1Venue_Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Venue_Page_0_document_0_Desktop.png",
        "label": "1.Venue Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Venue/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "6.67",
          "analysisTime": 621
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_1Venue_Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Venue_Page_CLICK_SubMenu_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_1Venue_Page_CLICK_SubMenu_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Venue_Page_CLICK_SubMenu_0_document_0_Desktop.png",
        "label": "1.Venue Page CLICK SubMenu",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Venue/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.26",
          "analysisTime": 667
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_1Venue_Page_CLICK_SubMenu_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Venue_Page_CLICK_More_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_1Venue_Page_CLICK_More_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Venue_Page_CLICK_More_0_document_0_Desktop.png",
        "label": "1.Venue Page CLICK More",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Venue/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.18",
          "analysisTime": 624
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_1Venue_Page_CLICK_More_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Venue_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_1Venue_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Venue_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "label": "1.Venue Page CLICK ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Venue/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "6.35",
          "analysisTime": 630
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_1Venue_Page_CLICK_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Venue_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_1Venue_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Venue_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "label": "1.Venue Page HOVER ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Venue/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "6.73",
          "analysisTime": 608
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_1Venue_Page_HOVER_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Venue_Page_HOVER_NewBtn_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_1Venue_Page_HOVER_NewBtn_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Venue_Page_HOVER_NewBtn_0_document_0_Desktop.png",
        "label": "1.Venue Page HOVER NewBtn",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Venue/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "6.66",
          "analysisTime": 581
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_1Venue_Page_HOVER_NewBtn_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Venue_Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_1Venue_Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Venue_Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png",
        "label": "1.Venue Page CLICK Indicatör NameAreas",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Venue/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "6.69",
          "analysisTime": 627
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_1Venue_Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Venue_Page_NewVenue_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_1Venue_Page_NewVenue_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Venue_Page_NewVenue_0_document_0_Desktop.png",
        "label": "1.Venue Page NewVenue",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/VenueAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -54
          },
          "misMatchPercentage": "22.84",
          "analysisTime": 860
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_1Venue_Page_NewVenue_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Venue_Page_NewVenue_CLICK_CompetitionArea_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_1Venue_Page_NewVenue_CLICK_CompetitionArea_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Venue_Page_NewVenue_CLICK_CompetitionArea_0_document_0_Desktop.png",
        "label": "1.Venue Page NewVenue CLICK CompetitionArea",
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/VenueAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -54
          },
          "misMatchPercentage": "22.18",
          "analysisTime": 824
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_1Venue_Page_NewVenue_CLICK_CompetitionArea_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Venue_Page_NewVenue_CLICK_WriteNewArea_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_1Venue_Page_NewVenue_CLICK_WriteNewArea_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Venue_Page_NewVenue_CLICK_WriteNewArea_0_document_0_Desktop.png",
        "label": "1.Venue Page NewVenue CLICK WriteNewArea",
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/VenueAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -54
          },
          "misMatchPercentage": "22.90",
          "analysisTime": 816
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_1Venue_Page_NewVenue_CLICK_WriteNewArea_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Venue_Page_NewVenue_CLICK_Country_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_1Venue_Page_NewVenue_CLICK_Country_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Venue_Page_NewVenue_CLICK_Country_0_document_0_Desktop.png",
        "label": "1.Venue Page NewVenue CLICK Country",
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/VenueAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -54
          },
          "misMatchPercentage": "25.11",
          "analysisTime": 524
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_1Venue_Page_NewVenue_CLICK_Country_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Venue_Page_NewVenue_Empty_Error_Messages_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_1Venue_Page_NewVenue_Empty_Error_Messages_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Venue_Page_NewVenue_Empty_Error_Messages_0_document_0_Desktop.png",
        "label": "1.Venue Page NewVenue Empty Error Messages",
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/VenueAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -54
          },
          "misMatchPercentage": "22.66",
          "analysisTime": 836
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_1Venue_Page_NewVenue_Empty_Error_Messages_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Venue_Page_CLICK_InfoBoxes_0__0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_1Venue_Page_CLICK_InfoBoxes_0__0_Desktop.png",
        "selector": "",
        "fileName": "Test_1Venue_Page_CLICK_InfoBoxes_0__0_Desktop.png",
        "label": "1.Venue Page CLICK InfoBoxes",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/VenueAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "engineErrorMsg": "Evaluation failed: TypeError: Cannot read property 'click' of null\n    at __puppeteer_evaluation_script__:1:86",
        "error": "Reference file not found /srv/www/mnm/backstop_data/html_report/AllTest/ReferenceDesktop/Test_1Venue_Page_CLICK_InfoBoxes_0__0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Schedule_Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_2Schedule_Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Schedule_Page_0_document_0_Desktop.png",
        "label": "2.Schedule Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Schedule/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "6.81",
          "analysisTime": 638
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_2Schedule_Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Schedule_Page_CLICK_SubMenu_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_2Schedule_Page_CLICK_SubMenu_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Schedule_Page_CLICK_SubMenu_0_document_0_Desktop.png",
        "label": "2.Schedule Page CLICK SubMenu",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Schedule/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.38",
          "analysisTime": 668
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_2Schedule_Page_CLICK_SubMenu_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Schedule_Page_CLICK_More_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_2Schedule_Page_CLICK_More_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Schedule_Page_CLICK_More_0_document_0_Desktop.png",
        "label": "2.Schedule Page CLICK More",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Schedule/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.47",
          "analysisTime": 681
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_2Schedule_Page_CLICK_More_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Schedule_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_2Schedule_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Schedule_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "label": "2.Schedule Page CLICK ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Schedule/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "6.47",
          "analysisTime": 597
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_2Schedule_Page_CLICK_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Schedule_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_2Schedule_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Schedule_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "label": "2.Schedule Page HOVER ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Schedule/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "6.88",
          "analysisTime": 652
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_2Schedule_Page_HOVER_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Schedule_Page_HOVER_NewBtn_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_2Schedule_Page_HOVER_NewBtn_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Schedule_Page_HOVER_NewBtn_0_document_0_Desktop.png",
        "label": "2.Schedule Page HOVER NewBtn",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Schedule/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "6.81",
          "analysisTime": 689
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_2Schedule_Page_HOVER_NewBtn_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Schedule_Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_2Schedule_Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Schedule_Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png",
        "label": "2.Schedule Page CLICK Indicatör NameAreas",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Schedule/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.35",
          "analysisTime": 696
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_2Schedule_Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Schedule_Page_NewSchedule_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_2Schedule_Page_NewSchedule_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Schedule_Page_NewSchedule_0_document_0_Desktop.png",
        "label": "2.Schedule Page NewSchedule",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ScheduleAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "9.86",
          "analysisTime": 750
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_2Schedule_Page_NewSchedule_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Schedule_Page_NewSchedule_Empty_Error_Messages_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_2Schedule_Page_NewSchedule_Empty_Error_Messages_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Schedule_Page_NewSchedule_Empty_Error_Messages_0_document_0_Desktop.png",
        "label": "2.Schedule Page NewSchedule Empty Error Messages",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ScheduleAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "11.03",
          "analysisTime": 652
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_2Schedule_Page_NewSchedule_Empty_Error_Messages_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Schedule_Page_NewSchedule_CLICK_Type_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_2Schedule_Page_NewSchedule_CLICK_Type_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Schedule_Page_NewSchedule_CLICK_Type_0_document_0_Desktop.png",
        "label": "2.Schedule Page NewSchedule CLICK Type",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ScheduleAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "11.44",
          "analysisTime": 700
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_2Schedule_Page_NewSchedule_CLICK_Type_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Schedule_Page_NewSchedule_CLICK_Venue_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_2Schedule_Page_NewSchedule_CLICK_Venue_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Schedule_Page_NewSchedule_CLICK_Venue_0_document_0_Desktop.png",
        "label": "2.Schedule Page NewSchedule CLICK Venue",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ScheduleAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.40",
          "analysisTime": 670
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_2Schedule_Page_NewSchedule_CLICK_Venue_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Schedule_Page_NewSchedule_CLICK_StartDate_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_2Schedule_Page_NewSchedule_CLICK_StartDate_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Schedule_Page_NewSchedule_CLICK_StartDate_0_document_0_Desktop.png",
        "label": "2.Schedule Page NewSchedule CLICK StartDate",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.3,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ScheduleAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "11.69",
          "analysisTime": 706
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_2Schedule_Page_NewSchedule_CLICK_StartDate_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Schedule_Page_NewSchedule_CLICK_EndDate_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_2Schedule_Page_NewSchedule_CLICK_EndDate_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Schedule_Page_NewSchedule_CLICK_EndDate_0_document_0_Desktop.png",
        "label": "2.Schedule Page NewSchedule CLICK EndDate",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.3,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ScheduleAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "11.22",
          "analysisTime": 745
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_2Schedule_Page_NewSchedule_CLICK_EndDate_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Schedule_Page_NewSchedule_CLICK_Info_Boxes_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190531-150453/Test_2Schedule_Page_NewSchedule_CLICK_Info_Boxes_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Schedule_Page_NewSchedule_CLICK_Info_Boxes_0_document_0_Desktop.png",
        "label": "2.Schedule Page NewSchedule CLICK Info Boxes",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/ScheduleAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "10.89",
          "analysisTime": 112
        },
        "diffImage": "../AllTest/TestDesktop/20190531-150453/failed_diff_Test_2Schedule_Page_NewSchedule_CLICK_Info_Boxes_0_document_0_Desktop.png"
      },
      "status": "fail"
    }
  ],
  "id": "Test"
});