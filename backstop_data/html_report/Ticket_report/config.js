report({
  "testSuite": "BackstopJS",
  "tests": [
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Ticket_Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_1Ticket_Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Ticket_Page_0_document_0_Desktop.png",
        "label": "1.Ticket Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Ticket/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "6.95",
          "analysisTime": 461
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233431/failed_diff_Test_1Ticket_Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Ticket_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_1Ticket_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Ticket_Page_CLICK_ColumnChooser_0_document_0_Desktop.png",
        "label": "1.Ticket Page CLICK ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Ticket/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "6.57",
          "analysisTime": 538
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233431/failed_diff_Test_1Ticket_Page_CLICK_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Ticket_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_1Ticket_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Ticket_Page_HOVER_ColumnChooser_0_document_0_Desktop.png",
        "label": "1.Ticket Page HOVER ColumnChooser",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Ticket/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.02",
          "analysisTime": 535
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233431/failed_diff_Test_1Ticket_Page_HOVER_ColumnChooser_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Ticket_Page_CLICK_SubMenu_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_1Ticket_Page_CLICK_SubMenu_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Ticket_Page_CLICK_SubMenu_0_document_0_Desktop.png",
        "label": "1.Ticket Page CLICK SubMenu",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Ticket/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.38",
          "analysisTime": 554
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233431/failed_diff_Test_1Ticket_Page_CLICK_SubMenu_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Ticket_Page_CLICK_More_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_1Ticket_Page_CLICK_More_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Ticket_Page_CLICK_More_0_document_0_Desktop.png",
        "label": "1.Ticket Page CLICK More",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Ticket/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.85",
          "analysisTime": 510
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233431/failed_diff_Test_1Ticket_Page_CLICK_More_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Ticket_Page_HOVER_AddTicket_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_1Ticket_Page_HOVER_AddTicket_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Ticket_Page_HOVER_AddTicket_0_document_0_Desktop.png",
        "label": "1.Ticket Page HOVER AddTicket",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Ticket/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "6.94",
          "analysisTime": 681
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233431/failed_diff_Test_1Ticket_Page_HOVER_AddTicket_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Ticket_Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_1Ticket_Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Ticket_Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png",
        "label": "1.Ticket Page CLICK Indicatör NameAreas",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Ticket/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "7.28",
          "analysisTime": 570
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233431/failed_diff_Test_1Ticket_Page_CLICK_Indicatr_NameAreas_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_1Ticket_Page_CLICK__NameAreas_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_1Ticket_Page_CLICK__NameAreas_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_1Ticket_Page_CLICK__NameAreas_0_document_0_Desktop.png",
        "label": "1.Ticket Page CLICK  NameAreas",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/Ticket/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "6.95",
          "analysisTime": 438
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233431/failed_diff_Test_1Ticket_Page_CLICK__NameAreas_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Ticket_ADD_Page_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_2Ticket_ADD_Page_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Ticket_ADD_Page_0_document_0_Desktop.png",
        "label": "2.Ticket ADD Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/TicketAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -72
          },
          "misMatchPercentage": "8.18",
          "analysisTime": 582
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233431/failed_diff_Test_2Ticket_ADD_Page_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Ticket_ADD_Page_CLICK_TicketType_0_viewport_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_2Ticket_ADD_Page_CLICK_TicketType_0_viewport_0_Desktop.png",
        "selector": "viewport",
        "fileName": "Test_2Ticket_ADD_Page_CLICK_TicketType_0_viewport_0_Desktop.png",
        "label": "2.Ticket ADD Page CLICK TicketType",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/TicketAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "8.23",
          "analysisTime": 567
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233431/failed_diff_Test_2Ticket_ADD_Page_CLICK_TicketType_0_viewport_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Ticket_ADD_Page_CLICK_UserRole_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_2Ticket_ADD_Page_CLICK_UserRole_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Ticket_ADD_Page_CLICK_UserRole_0_document_0_Desktop.png",
        "label": "2.Ticket ADD Page CLICK UserRole",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/TicketAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -72
          },
          "misMatchPercentage": "5.49",
          "analysisTime": 729
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233431/failed_diff_Test_2Ticket_ADD_Page_CLICK_UserRole_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Ticket_ADD_Page_CLICK_Currency_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_2Ticket_ADD_Page_CLICK_Currency_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Ticket_ADD_Page_CLICK_Currency_0_document_0_Desktop.png",
        "label": "2.Ticket ADD Page CLICK Currency",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/TicketAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -72
          },
          "misMatchPercentage": "8.29",
          "analysisTime": 588
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233431/failed_diff_Test_2Ticket_ADD_Page_CLICK_Currency_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Ticket_ADD_Page_CLICK_StandartRegisterStartDate_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_2Ticket_ADD_Page_CLICK_StandartRegisterStartDate_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Ticket_ADD_Page_CLICK_StandartRegisterStartDate_0_document_0_Desktop.png",
        "label": "2.Ticket ADD Page CLICK StandartRegisterStartDate",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.07,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/TicketAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -72
          },
          "misMatchPercentage": "7.17",
          "analysisTime": 781
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233431/failed_diff_Test_2Ticket_ADD_Page_CLICK_StandartRegisterStartDate_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Ticket_ADD_Page_CLICK_EarlyRegisterStartDate_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_2Ticket_ADD_Page_CLICK_EarlyRegisterStartDate_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Ticket_ADD_Page_CLICK_EarlyRegisterStartDate_0_document_0_Desktop.png",
        "label": "2.Ticket ADD Page CLICK EarlyRegisterStartDate",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.07,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/TicketAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -72
          },
          "misMatchPercentage": "8.77",
          "analysisTime": 761
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233431/failed_diff_Test_2Ticket_ADD_Page_CLICK_EarlyRegisterStartDate_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Ticket_ADD_Page_CLICK_LateRegisterStartDate_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_2Ticket_ADD_Page_CLICK_LateRegisterStartDate_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Ticket_ADD_Page_CLICK_LateRegisterStartDate_0_document_0_Desktop.png",
        "label": "2.Ticket ADD Page CLICK LateRegisterStartDate",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.07,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/TicketAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -72
          },
          "misMatchPercentage": "8.70",
          "analysisTime": 666
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233431/failed_diff_Test_2Ticket_ADD_Page_CLICK_LateRegisterStartDate_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Ticket_ADD_Page_CLICK_StandartRegisterEndDate_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_2Ticket_ADD_Page_CLICK_StandartRegisterEndDate_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Ticket_ADD_Page_CLICK_StandartRegisterEndDate_0_document_0_Desktop.png",
        "label": "2.Ticket ADD Page CLICK StandartRegisterEndDate",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.07,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/TicketAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -72
          },
          "misMatchPercentage": "7.54",
          "analysisTime": 692
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233431/failed_diff_Test_2Ticket_ADD_Page_CLICK_StandartRegisterEndDate_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Ticket_ADD_Page_CLICK_EarlyRegisterEndDate_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_2Ticket_ADD_Page_CLICK_EarlyRegisterEndDate_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Ticket_ADD_Page_CLICK_EarlyRegisterEndDate_0_document_0_Desktop.png",
        "label": "2.Ticket ADD Page CLICK EarlyRegisterEndDate",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.07,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/TicketAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -72
          },
          "misMatchPercentage": "8.90",
          "analysisTime": 542
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233431/failed_diff_Test_2Ticket_ADD_Page_CLICK_EarlyRegisterEndDate_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Ticket_ADD_Page_CLICK_LateRegisterEndDate_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_2Ticket_ADD_Page_CLICK_LateRegisterEndDate_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Ticket_ADD_Page_CLICK_LateRegisterEndDate_0_document_0_Desktop.png",
        "label": "2.Ticket ADD Page CLICK LateRegisterEndDate",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.07,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/TicketAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -72
          },
          "misMatchPercentage": "8.84",
          "analysisTime": 561
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233431/failed_diff_Test_2Ticket_ADD_Page_CLICK_LateRegisterEndDate_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Ticket_ADD_Page_CLICK_InfoBoxesANDToggle_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_2Ticket_ADD_Page_CLICK_InfoBoxesANDToggle_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Ticket_ADD_Page_CLICK_InfoBoxesANDToggle_0_document_0_Desktop.png",
        "label": "2.Ticket ADD Page CLICK InfoBoxesANDToggle",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/TicketAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -72
          },
          "misMatchPercentage": "10.29",
          "analysisTime": 705
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233431/failed_diff_Test_2Ticket_ADD_Page_CLICK_InfoBoxesANDToggle_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Ticket_Page_HOVER_BackBtn_0_rowmnm-buttons_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_2Ticket_Page_HOVER_BackBtn_0_rowmnm-buttons_0_Desktop.png",
        "selector": ".row.mnm-buttons",
        "fileName": "Test_2Ticket_Page_HOVER_BackBtn_0_rowmnm-buttons_0_Desktop.png",
        "label": "2.Ticket Page HOVER BackBtn",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/TicketAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Ticket_Page_HOVER_SaveBtn_0_rowmnm-buttons_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_2Ticket_Page_HOVER_SaveBtn_0_rowmnm-buttons_0_Desktop.png",
        "selector": ".row.mnm-buttons",
        "fileName": "Test_2Ticket_Page_HOVER_SaveBtn_0_rowmnm-buttons_0_Desktop.png",
        "label": "2.Ticket Page HOVER SaveBtn",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/TicketAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_2Ticket_Page_NewVenue_Empty_Error_Messages_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190529-233431/Test_2Ticket_Page_NewVenue_Empty_Error_Messages_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_2Ticket_Page_NewVenue_Empty_Error_Messages_0_document_0_Desktop.png",
        "label": "2.Ticket Page NewVenue Empty Error Messages",
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/TicketAdd?eventId=29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -72
          },
          "misMatchPercentage": "8.30",
          "analysisTime": 653
        },
        "diffImage": "../AllTest/TestDesktop/20190529-233431/failed_diff_Test_2Ticket_Page_NewVenue_Empty_Error_Messages_0_document_0_Desktop.png"
      },
      "status": "fail"
    }
  ],
  "id": "Test"
});