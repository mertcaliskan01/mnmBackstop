report({
  "testSuite": "BackstopJS",
  "tests": [
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_MatchResult_Page_CLICK_New_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190611-165901/Test_MatchResult_Page_CLICK_New_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_MatchResult_Page_CLICK_New_0_document_0_Desktop.png",
        "label": "MatchResult Page CLICK [New]",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/MatchResult/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "10.19",
          "analysisTime": 628
        },
        "diffImage": "../AllTest/TestDesktop/20190611-165901/failed_diff_Test_MatchResult_Page_CLICK_New_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_MatchResult_Page_CLICK_Country_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190611-165901/Test_MatchResult_Page_CLICK_Country_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_MatchResult_Page_CLICK_Country_0_document_0_Desktop.png",
        "label": "MatchResult Page CLICK [Country]",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/MatchResult/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "10.28",
          "analysisTime": 623
        },
        "diffImage": "../AllTest/TestDesktop/20190611-165901/failed_diff_Test_MatchResult_Page_CLICK_Country_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_MatchResult_Page_CLICK_Division_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190611-165901/Test_MatchResult_Page_CLICK_Division_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_MatchResult_Page_CLICK_Division_0_document_0_Desktop.png",
        "label": "MatchResult Page CLICK [Division]",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/MatchResult/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "9.61",
          "analysisTime": 613
        },
        "diffImage": "../AllTest/TestDesktop/20190611-165901/failed_diff_Test_MatchResult_Page_CLICK_Division_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_MatchResult_Page_CLICK_Area_0_document_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190611-165901/Test_MatchResult_Page_CLICK_Area_0_document_0_Desktop.png",
        "selector": "document",
        "fileName": "Test_MatchResult_Page_CLICK_Area_0_document_0_Desktop.png",
        "label": "MatchResult Page CLICK [Area]",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/MatchResult/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "9.72",
          "analysisTime": 623
        },
        "diffImage": "../AllTest/TestDesktop/20190611-165901/failed_diff_Test_MatchResult_Page_CLICK_Area_0_document_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_MatchResult_Page_CLICK_Status_0_viewport_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190611-165901/Test_MatchResult_Page_CLICK_Status_0_viewport_0_Desktop.png",
        "selector": "viewport",
        "fileName": "Test_MatchResult_Page_CLICK_Status_0_viewport_0_Desktop.png",
        "label": "MatchResult Page CLICK [Status]",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/MatchResult/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "10.36",
          "analysisTime": 626
        },
        "diffImage": "../AllTest/TestDesktop/20190611-165901/failed_diff_Test_MatchResult_Page_CLICK_Status_0_viewport_0_Desktop.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../AllTest/ReferenceDesktop/Test_MatchResult_Page_Hover_SearchBtn_0_mnm-save-button_0_Desktop.png",
        "test": "../AllTest/TestDesktop/20190611-165901/Test_MatchResult_Page_Hover_SearchBtn_0_mnm-save-button_0_Desktop.png",
        "selector": "#mnm-save-button",
        "fileName": "Test_MatchResult_Page_Hover_SearchBtn_0_mnm-save-button_0_Desktop.png",
        "label": "MatchResult Page Hover [SearchBtn]",
        "requireSameDimensions": true,
        "misMatchThreshold": 0,
        "url": "https://dev-new.mynextmatch.com/Event/EventManage/MatchResult/29",
        "expect": 0,
        "viewportLabel": "Desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    }
  ],
  "id": "Test"
});